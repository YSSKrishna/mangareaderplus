# MangaReader+

Eventhough there are some manga readers available online, I was not satisfied with their limitations. 
So i took this existing project **MangaReader** from sourceforge as my base project.

My goal is to make an enhanced MangaReader. Hope you like it. 


## Overview:
- It's an image viewer program tailored to be a manga viewer.
- Its usage is similar to OneManga's viewer and MangaFox's viewer.
- Uses keyboard shortcuts to move about the pages.
- Can adjust the viewing area's display size(zoom of the image).
- Can recursively read subdirectories and display all those who have
  valid image files to display.

## Installation:
- Binary Zip File:
  + Unpack the binary zip file to a folder of your choice.
  + Start MangaReader.exe

- Source Code:
  + REQUIRES : Python 2.6
  + REQUIRES : wxPython 2.8
  + REQUIRES : PIL 1.1.6
  + Start MangaReader.py

## Keyboard Controls:
- "O" key for Open Folder
- Left Arrow key for previous page (jump to previous chapter if 
  beginning of current chapter)
- Right Arrow key for next page (jump to next chapter if end of 
  current chapter)
- Spacebar for scroll down/next page (if scrolled all the way dow, 
  it'll jump to next page)
- "X" key to quit program
- "A" key to minimize program
- Click on Image for next page (if the image was resized since it was 
  bigger than allowed view area, then clicking on the image will make 
  the image be resized to fit the width of the view area (like for 
  2-page images) then it'll jump to next page on next click.)
