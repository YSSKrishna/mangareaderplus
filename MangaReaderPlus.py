#import os
from os import walk, path
import wx
import cStringIO
#import sys
from sys import stderr, platform, executable
from math import ceil
from wx.lib.buttons import GenBitmapTextButton
from wx.lib.buttons import GenBitmapButton
from PIL import Image
#from win32api import GetModuleFileName, GetModuleHandle
from Images import CrossMark_png, First_png, FolderOpen_png, Last_png, Next_png, Next_Small_png, Prev_png, Prev_Small_png

#sys.stderr = open("MangaReader.log", "w")
stderr = open("MangaReader.log", "w")
oImg_CrossMark = None
oImg_First = None
oImg_FolderOpen = None
oImg_Last = None
oImg_Next = None
oImg_NextSmall = None
oImg_Pref = None
oImg_PrevSmall = None

def msgBox(*sArgs):
    sText = r""
    for sToken in sArgs:
        sText = sText + " " + unicode(sToken)
    sText = sText[1:]
    
    wx.MessageBox(sText)

class StatusWindow(wx.Dialog):
    oStatus = None

    def __init__(self, parent, ID, title):
        oScreen = wx.Display().GetClientArea()
        wx.Dialog.__init__(self, parent, ID, title, wx.Point(((oScreen.GetWidth() / 2) - 275), ((oScreen.GetHeight() / 2) - 37)), wx.Size(550, 55), wx.FRAME_SHAPED)
        self.SetShape(self.GetRoundShape(545, 45, 10))
        oBox = wx.BoxSizer(wx.VERTICAL)
        
        oLabelFont = wx.Font(15, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Helvetica")
        
        StatusWindow.oStatus = wx.StaticText(self, -1, "1", wx.DefaultPosition, wx.Size(530, 25), wx.CENTER | wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE)
        StatusWindow.oStatus.SetFont(oLabelFont)

        oBox.Add(StatusWindow.oStatus, 0, wx.ALL, 10)
        
        self.SetAutoLayout(True)
        self.SetSizer(oBox)
        self.Layout()
    
    def GetRoundBitmap( self, w, h, radius ):
        maskColor = wx.Color(0,0,0)
        shownColor = wx.Color(5,5,5)
        b = wx.EmptyBitmap(w,h)
        dc = wx.MemoryDC()
        dc.SelectObject(b)    
        dc.SetBrush(wx.Brush(maskColor))
        dc.DrawRectangle(0,0,w,h)
        dc.SetBrush(wx.Brush(shownColor))
        dc.DrawRoundedRectangle(0,0,w,h,radius)
        dc.SelectObject(wx.NullBitmap)
        b.SetMaskColour(maskColor)
        return b

    def GetRoundShape( self, w, h, r ):
        return wx.RegionFromBitmap( self.GetRoundBitmap(w,h,r) )
    
    def SetStatus(self, *sArgs):
        sText = r""
        for sToken in sArgs:
            sText = sText + " " + unicode(sToken)
        sText = sText[1:]
        
        StatusWindow.oStatus.SetLabel(sText)

class ReaderWindow(wx.Frame):
    oMainPanel = None
    oMovePanel = None
    oImagePanel = None
    oControlPanel = None

    oView = None
    oChapter = None
    oPage = None

    oImage = None
    oImageArea = None
    oDisplayBitmap = None

    oCurrent = None
    
    sFolderPath = r""
    sBasePath = r""

    sChapterList = []
    sChapNameList = []
    sPageList = []
    bPrev = False
    sValidExt = ["JPG", "JPEG", "GIF", "BMP", "PNG"]
    
    nViewArea = 75
    nOrigWidth = 0
    nOrigHeight = 0
    nImageState = 0

    oStatus = None
    
    def __init__(self, parent, ID, title):
        IDP_MAIN = 101
        IDP_MOVE = 102
        IDP_IMAGE = 103
        IDP_CTRL = 104
        
        IDC_OPEN = 201
        IDC_CLOSE = 202
        IDC_VIEWLABEL = 203
        IDC_VIEW = 204
        
        IDC_CHAPTER = 205
        IDC_CHAPPREV = 206
        IDC_CHAPNEXT = 207
        IDC_PAGE = 208
        IDC_PAGEPREV = 209
        IDC_PAGENEXT = 210
        
        IDC_DISPLAY = 211
        
        IDC_FIRST   = 212
        IDC_PREV    = 213
        IDC_CURRENT = 214
        IDC_NEXT    = 215
        IDC_LAST    = 216
        
        screen = wx.Display()
        wx.Frame.__init__(self, parent, ID, title, wx.Point(0, 0), screen.GetClientArea().GetSize(), wx.DEFAULT_FRAME_STYLE)
        self.SetMinSize(wx.Size(550,500))
        self.SetBackgroundColour("#1c1c1c")
        
        #if sys.platform == 'win32':
        if platform == 'win32':
            #sExeName = sys.executable
            sExeName = executable
            oIcon = wx.Icon(sExeName, wx.BITMAP_TYPE_ICO)
            self.SetIcon(oIcon)
        
        vStream = cStringIO.StringIO(CrossMark_png.data)
        oImg_CrossMark = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(First_png.data)
        oImg_First = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(FolderOpen_png.data)
        oImg_FolderOpen = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(Last_png.data)
        oImg_Last = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(Next_png.data)
        oImg_Next = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(Next_Small_png.data)
        oImg_NextSmall = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(Prev_png.data)
        oImg_Prev = wx.BitmapFromImage(wx.ImageFromStream(vStream))
        vStream = cStringIO.StringIO(Prev_Small_png.data)
        oImg_PrevSmall = wx.BitmapFromImage(wx.ImageFromStream(vStream))        

        oButtonFont = wx.Font(15, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Helvetica")
        oLabelFont = wx.Font(15, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Helvetica")
        oSelectFont = wx.Font(12, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Helvetica")
        
        oBox = wx.BoxSizer(wx.VERTICAL)
        
        # Top Main Panel
        ReaderWindow.oMainPanel = wx.Panel(self, IDP_MAIN, wx.Point(0, 0), wx.Size(-1, 40))
        oMainBox  = wx.BoxSizer(wx.HORIZONTAL)
        oMainBox1 = wx.BoxSizer(wx.HORIZONTAL)
        oMainBox2 = wx.BoxSizer(wx.HORIZONTAL)
        
        oOpen = GenBitmapTextButton(ReaderWindow.oMainPanel, IDC_OPEN, oImg_FolderOpen, " Open Folder", wx.DefaultPosition, wx.Size(175, 35))
        oOpen.SetFont(oButtonFont)
        oClose = GenBitmapTextButton(ReaderWindow.oMainPanel, IDC_CLOSE, oImg_CrossMark, "Close ", wx.DefaultPosition, wx.Size(125, 35))
        oClose.SetFont(oButtonFont)
        
        oViewLabel = wx.StaticText(ReaderWindow.oMainPanel, IDC_VIEWLABEL, "View Area : ")
        oViewLabel.SetFont(oLabelFont)
        oViewLabel.SetForegroundColour("#ffffaf")
        
        nViewList = ["50%", "55%", "60%", "65%", "70%", "75%", "80%", "85%", "90%", "95%"]
        ReaderWindow.oView = wx.ComboBox(ReaderWindow.oMainPanel, IDC_VIEW, "75%", wx.DefaultPosition, wx.DefaultSize, nViewList, wx.CB_DROPDOWN | wx.CB_READONLY | wx.CB_SORT)
        ReaderWindow.oView.SetFont(oSelectFont)
        
        oMainBox1.Add(oOpen, 0, wx.ALIGN_LEFT | wx.RIGHT, 10)
        oMainBox1.Add(oClose, 0, wx.ALIGN_LEFT)
        oMainBox2.Add(oViewLabel, 0)
        oMainBox2.Add(ReaderWindow.oView, 0, wx.ALIGN_LEFT)
        oMainBox.Add(oMainBox1, 8, wx.LEFT | wx.TOP | wx.ALIGN_CENTER_VERTICAL, 10)
        oMainBox.Add(oMainBox2, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT | wx.RIGHT | wx.TOP, 10)
        
        ReaderWindow.oMainPanel.SetSizer(oMainBox)
        
        # Chapter/Page Movement Panel
        ReaderWindow.oMovePanel = wx.Panel(self, IDP_MOVE, wx.Point(0, -1), wx.Size(-1, 30))
        oMoveBox = wx.BoxSizer(wx.HORIZONTAL)
        oMoveBox1 = wx.BoxSizer(wx.HORIZONTAL)
        oMoveBox2 = wx.BoxSizer(wx.HORIZONTAL)
        
        oMovSize = wx.Size(26, 26)
        oChapLabel = wx.StaticText(ReaderWindow.oMovePanel, -1, unichr(0x2022) + " Browsing : ")
        ReaderWindow.oChapter = wx.ComboBox(ReaderWindow.oMovePanel, IDC_CHAPTER, "", wx.DefaultPosition, wx.DefaultSize, [], wx.CB_DROPDOWN | wx.CB_READONLY)
        oChapPrev = GenBitmapButton(ReaderWindow.oMovePanel, IDC_CHAPPREV, oImg_PrevSmall, wx.DefaultPosition, oMovSize)
        oChapNext = GenBitmapButton(ReaderWindow.oMovePanel, IDC_CHAPNEXT, oImg_NextSmall, wx.DefaultPosition, oMovSize)
        oPageLabel = wx.StaticText(ReaderWindow.oMovePanel, -1, "Page : ")
        ReaderWindow.oPage = wx.ComboBox(ReaderWindow.oMovePanel, IDC_PAGE, "0000", wx.DefaultPosition, wx.DefaultSize, ["0000"], wx.CB_DROPDOWN | wx.CB_READONLY)
        oPagePrev = GenBitmapButton(ReaderWindow.oMovePanel, IDC_PAGEPREV, oImg_PrevSmall, wx.DefaultPosition, oMovSize)
        oPageNext = GenBitmapButton(ReaderWindow.oMovePanel, IDC_PAGENEXT, oImg_NextSmall, wx.DefaultPosition, oMovSize)
        
        ReaderWindow.oChapter.SetFont(oSelectFont)
        ReaderWindow.oPage.SetFont(oSelectFont)
        oChapLabel.SetFont(oLabelFont)
        oPageLabel.SetFont(oLabelFont)
        oChapPrev.SetFont(oButtonFont)
        oChapNext.SetFont(oButtonFont)
        oPagePrev.SetFont(oButtonFont)
        oPageNext.SetFont(oButtonFont)
        oChapLabel.SetForegroundColour("#ffffaf")
        oPageLabel.SetForegroundColour("#ffffaf")
        
        oMoveBox1.Add(oChapLabel, 0, wx.ALIGN_LEFT)
        oMoveBox1.Add(ReaderWindow.oChapter, 6, wx.ALIGN_LEFT | wx.RIGHT, 5)
        oMoveBox1.Add(oChapPrev, 0, wx.ALIGN_LEFT)
        oMoveBox1.Add(oChapNext, 0, wx.ALIGN_LEFT)
        oMoveBox2.Add(oPageLabel, 0, wx.ALIGN_LEFT)
        oMoveBox2.Add(ReaderWindow.oPage, 0, wx.ALIGN_LEFT | wx.RIGHT, 5)
        oMoveBox2.Add(oPagePrev, 0, wx.ALIGN_LEFT)
        oMoveBox2.Add(oPageNext, 0, wx.ALIGN_LEFT)
        oMoveBox.Add(oMoveBox1, 6, wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 10)
        oMoveBox.Add(oMoveBox2, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, 10)
        
        ReaderWindow.oMovePanel.SetSizer(oMoveBox)
        
        # Image Panel
        ReaderWindow.oImagePanel = wx.Panel(self, IDP_IMAGE, wx.Point(0, -1), wx.Size(-1. -1))
        oImageBox = wx.BoxSizer(wx.HORIZONTAL)
        oScrollBox = wx.BoxSizer(wx.VERTICAL)
        
        ReaderWindow.oImageArea = wx.ScrolledWindow(ReaderWindow.oImagePanel)
        ReaderWindow.oImageArea.SetBackgroundColour("#1c1c1c")
        ReaderWindow.oImageArea.SetScrollbars(20, 20, 55, 40)
        
        oBitmap = wx.EmptyBitmap(1, 1)
        ReaderWindow.oDisplayBitmap = wx.StaticBitmap(ReaderWindow.oImageArea, IDC_DISPLAY, oBitmap)
        oScrollBox.Add(ReaderWindow.oDisplayBitmap, 10, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        
        ReaderWindow.oImageArea.EnableScrolling(True, True)
        ReaderWindow.oImageArea.SetScrollbars(20, 20, 55, 40)
        ReaderWindow.oImageArea.SetAutoLayout(True)
        ReaderWindow.oImageArea.SetSizer(oScrollBox)
        ReaderWindow.oImageArea.Layout()        
        
        oScrollBox.SetVirtualSizeHints(ReaderWindow.oImageArea)
        oImageBox.Add(ReaderWindow.oImageArea, 10, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER | wx.ALIGN_CENTER_VERTICAL, 10)
        
        ReaderWindow.oImagePanel.SetAutoLayout(True)
        ReaderWindow.oImagePanel.SetSizer(oImageBox)
        ReaderWindow.oImagePanel.Layout()
        
        # Bottom Control Panel
        ReaderWindow.oControlPanel = wx.Panel(self, IDP_CTRL, wx.Point(0, -1), wx.Size(-1, 35))
        oControlBox = wx.GridSizer(1, 5, 0, 10)
        
        oControlSize = wx.Size(75, 35)
        oFirst = GenBitmapTextButton(ReaderWindow.oControlPanel, IDC_FIRST, oImg_First, "First", wx.DefaultPosition, oControlSize)
        oPrev = GenBitmapTextButton(ReaderWindow.oControlPanel, IDC_PREV, oImg_Prev, "Previous", wx.DefaultPosition, oControlSize)
        ReaderWindow.oCurrent = wx.StaticText(ReaderWindow.oControlPanel, IDC_CURRENT, "0 / 0")
        oNext = GenBitmapTextButton(ReaderWindow.oControlPanel, IDC_NEXT, oImg_Next, "Next", wx.DefaultPosition, oControlSize)
        oLast = GenBitmapTextButton(ReaderWindow.oControlPanel, IDC_LAST, oImg_Last, "Last", wx.DefaultPosition, oControlSize)
        oFirst.SetFont(oButtonFont)
        oPrev.SetFont(oButtonFont)
        oNext.SetFont(oButtonFont)
        oLast.SetFont(oButtonFont)
        ReaderWindow.oCurrent.SetFont(oLabelFont)
        ReaderWindow.oCurrent.SetForegroundColour("#87afdf")
        
        oControlBox.Add(oFirst, 2, wx.LEFT | wx.EXPAND, 10)
        oControlBox.Add(oPrev, 2, wx.EXPAND)
        oControlBox.Add(ReaderWindow.oCurrent, 2, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        oControlBox.Add(oNext, 2, wx.EXPAND)
        oControlBox.Add(oLast, 2, wx.RIGHT | wx.EXPAND, 10)
        
        ReaderWindow.oControlPanel.SetSizer(oControlBox)
        
        oBox.Add(ReaderWindow.oMainPanel, 0, wx.BOTTOM | wx.EXPAND, 10)
        oBox.Add(ReaderWindow.oMovePanel, 0, wx.BOTTOM | wx.EXPAND, 5)
        oBox.Add(ReaderWindow.oImagePanel, 7, wx.BOTTOM | wx.EXPAND, 5)
        oBox.Add(ReaderWindow.oControlPanel, 0, wx.BOTTOM | wx.EXPAND, 10)
        
        self.SetAutoLayout(True)
        self.SetSizer(oBox)
        self.Layout()
        
        self.Bind(wx.EVT_BUTTON, self.OnQuit, id=IDC_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnOpenFolder, id=IDC_OPEN)
        self.Bind(wx.EVT_BUTTON, self.OnPageNext, id=IDC_PAGENEXT)
        self.Bind(wx.EVT_BUTTON, self.OnPageNext, id=IDC_NEXT)
        self.Bind(wx.EVT_BUTTON, self.OnPagePrev, id=IDC_PAGEPREV)
        self.Bind(wx.EVT_BUTTON, self.OnPagePrev, id=IDC_PREV)
        self.Bind(wx.EVT_BUTTON, self.OnPageFirst, id=IDC_FIRST)
        self.Bind(wx.EVT_BUTTON, self.OnPageLast, id=IDC_LAST)
        self.Bind(wx.EVT_BUTTON, self.OnChapPrev, id=IDC_CHAPPREV)
        self.Bind(wx.EVT_BUTTON, self.OnChapNext, id=IDC_CHAPNEXT)
        
        self.Bind(wx.EVT_COMBOBOX, self.OnChapterSelect, id=IDC_CHAPTER)        
        self.Bind(wx.EVT_COMBOBOX, self.OnPageSelect, id=IDC_PAGE)
        self.Bind(wx.EVT_COMBOBOX, self.OnViewSelect, id=IDC_VIEW)
        
        ReaderWindow.oDisplayBitmap.Bind(wx.EVT_LEFT_UP, self.OnImageClick, id=IDC_DISPLAY)
        
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_OPEN)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_CLOSE)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_PAGEPREV)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_PAGENEXT)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_CHAPPREV)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_CHAPNEXT)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_PREV)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_NEXT)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_FIRST)
        self.Bind(wx.EVT_SET_FOCUS, self.OnGetFocus, id=IDC_LAST)
        
        ReaderWindow.oImageArea.Bind(wx.EVT_KEY_DOWN, self.OnKeyPress)
        
        ReaderWindow.oImageArea.SetFocus()
        self.HideControls()
        
        ReaderWindow.oStatus = StatusWindow(self, -1, "MangaReader - Status Message")
        self.SetStatus("To begin, click on \"Open Folder\" to start browsing!")

    def OnGetFocus(self, event):
        ReaderWindow.oImageArea.SetFocus()
        
    def OnKeyPress(self, event):
        nKeyCode = event.GetKeyCode()
        if nKeyCode == ord("O") or nKeyCode == ord("o"):
            self.OnOpenFolder(None)
        if nKeyCode == wx.WXK_ESCAPE or nKeyCode == ord("X") or nKeyCode == ord("x"):
            self.Close()
        if nKeyCode == ord("A") or nKeyCode == ord("a"):
            self.Iconize()
        if nKeyCode == wx.WXK_RIGHT:
            self.OnImageClick(None)
        if nKeyCode == wx.WXK_LEFT:
            self.OnPagePrev(None)
        if nKeyCode == wx.WXK_SPACE:
            oArea = ReaderWindow.oImageArea
            nVUnits = ReaderWindow.oDisplayBitmap.GetSize().GetHeight() / oArea.GetScrollPixelsPerUnit()[1]
            nSUnits = oArea.GetSize().GetHeight() / oArea.GetScrollPixelsPerUnit()[1]
            nStep = int(nVUnits / ceil((nVUnits * 1.0) / nSUnits))
            nEnd = nVUnits - nSUnits
            nCurrent = oArea.GetViewStart()[1]            
            if nCurrent < nEnd:
                oArea.Scroll(-1, (nCurrent + nStep))
            else:
                self.OnPageNext(None)
        
        event.Skip()
        
    def OnQuit(self, event):
        self.Close()

    def OnOpenFolder(self, event):
        self.SetStatus("Choose a Folder...")
        oDialog = wx.DirDialog(self, "Choose a folder", ReaderWindow.sFolderPath, wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.DD_DIR_MUST_EXIST)
        if oDialog.ShowModal() == wx.ID_OK:
            ReaderWindow.sFolderPath = oDialog.GetPath()
            ReaderWindow.sBasePath = ""
            ReaderWindow.sChapterList = []
            ReaderWindow.sChapNameList = []
            self.CheckDir(ReaderWindow.sFolderPath)
            ReaderWindow.oChapter.Clear()
            ReaderWindow.oChapter.AppendItems(ReaderWindow.sChapNameList)
            ReaderWindow.oChapter.SetSelection(0)
            self.OnChapterSelect(ReaderWindow.oChapter)
            self.ShowControls()

    def CheckDir(self, sPath):        
        self.SetStatus("Checking", sPath, "...")
        sFiles = []
        sFolders = []
        #for sFPath, sFolders, sFiles in os.walk(sPath):            
        for sFPath, sFolders, sFiles in walk(sPath):            
            if (len(sFiles) > 0):
                bValid = False
                for i in sFiles:
                    #(sRoot, sExt) = os.path.splitext(i)
                    (sRoot, sExt) = path.splitext(i)
                    sExt = sExt[1:]
                    if sExt.upper() in ReaderWindow.sValidExt:
                        bValid = True
                        break
                if bValid:
                    ReaderWindow.sChapterList.append(sFPath)
                    if ReaderWindow.sFolderPath != "" and ReaderWindow.sBasePath == "":
                        if ReaderWindow.sFolderPath == sFPath:
                            #ReaderWindow.sBasePath = os.path.dirname(ReaderWindow.sFolderPath)
                            ReaderWindow.sBasePath = path.dirname(ReaderWindow.sFolderPath)
                        else:
                            ReaderWindow.sBasePath = ReaderWindow.sFolderPath
                    #ReaderWindow.sChapNameList.append(os.path.relpath(sFPath, ReaderWindow.sBasePath))
                    ReaderWindow.sChapNameList.append(path.relpath(sFPath, ReaderWindow.sBasePath))

    def ShowControls(self):
        ReaderWindow.oMovePanel.Show()
        ReaderWindow.oImagePanel.Show()
        ReaderWindow.oControlPanel.Show()

    def HideControls(self):
        ReaderWindow.oMovePanel.Hide()
        ReaderWindow.oImagePanel.Hide()
        ReaderWindow.oControlPanel.Hide()

    def OnChapterSelect(self, event):
        nItem = event.GetSelection()
        self.SetStatus("Reading", ReaderWindow.sChapNameList[nItem], "...")        
        sFiles = []
        #for sPath, sFolders, sFiles in os.walk(ReaderWindow.sChapterList[nItem]):            
        for sPath, sFolders, sFiles in walk(ReaderWindow.sChapterList[nItem]):            
            break
        if len(sFiles) > 0:        
            ReaderWindow.sPageList = []
            for i in sFiles:
                #(sRoot, sExt) = os.path.splitext(i)
                (sRoot, sExt) = path.splitext(i)
                sExt = sExt[1:]
                if sExt.upper() in ReaderWindow.sValidExt:
                    ReaderWindow.sPageList.append(i)
        if len(ReaderWindow.sPageList) > 0:            
            ReaderWindow.sPageList.sort()
            ReaderWindow.oPage.Clear()
            a = range(1, len(ReaderWindow.sPageList) + 1)
            b = []
            if a == []:
                a = [1]                
            for i in a:
                b.append(unicode(i))
            ReaderWindow.oPage.AppendItems(b)
            if not ReaderWindow.bPrev:
                ReaderWindow.oPage.SetSelection(0)
            else:
                ReaderWindow.bPrev = False
                ReaderWindow.oPage.SetSelection(len(ReaderWindow.sPageList) - 1)
            self.OnPageSelect(ReaderWindow.oPage)
        else:
            self.SetStatus("No Files to view on", ReaderWindow.sChapNameList[nItem])
            
    def OnPageSelect(self, event):
        nItem = event.GetSelection()
        self.SetStatus("Viewing Page", nItem + 1, "...")
        self.Freeze()        
        sFile = ReaderWindow.sChapterList[ReaderWindow.oChapter.GetSelection()] + "\\" + ReaderWindow.sPageList[nItem]
        self.SetImage(sFile)
        ReaderWindow.oImageArea.Scroll(-100, -100)
        ReaderWindow.oCurrent.SetLabel(unicode(nItem + 1) + " / " + unicode(len(ReaderWindow.sPageList)))
        ReaderWindow.oImageArea.SetFocus()
        self.Thaw()
        self.HideStatus()
        
    def OnViewSelect(self, event):
        nView = ReaderWindow.oView.GetValue()
        self.SetStatus("Changing View Area Size to", nView, "of the screen")
        ReaderWindow.nViewArea = int(nView[:-1])
        if ReaderWindow.oImagePanel.IsShown(): self.OnPageSelect(ReaderWindow.oPage)

    def DisplayImage(self, nWidth, nHeight):
        self.Freeze()
        
        ReaderWindow.oImage = ReaderWindow.oImage.convert("RGB")
        ReaderWindow.oImage = ReaderWindow.oImage.resize((nWidth, nHeight), Image.ANTIALIAS)
        
        oPImage = wx.EmptyImage(ReaderWindow.oImage.size[0], ReaderWindow.oImage.size[1])
        oPImage.SetData(ReaderWindow.oImage.convert('RGB').tostring())
        oPImage.SetAlphaData(ReaderWindow.oImage.convert("RGBA").tostring()[3::4])
        oBitmap = wx.BitmapFromImage(oPImage)        
        
        ReaderWindow.oDisplayBitmap.SetBitmap(oBitmap)
        ReaderWindow.oImageArea.SetVirtualSize(oBitmap.GetSize())
        ReaderWindow.oImageArea.Layout()
        ReaderWindow.oImageArea.Refresh()
        ReaderWindow.oImageArea.Update()        
        
        self.Thaw()
        
    def SetImage(self, sImageFile):        
        if ReaderWindow.oImage != None: del ReaderWindow.oImage
        ReaderWindow.oImage = Image.open(sImageFile)
        
        bShrink = False
        oScrSize = ReaderWindow.oImageArea.GetSize()
        
        nViewWidth = int(oScrSize.GetWidth() * ReaderWindow.nViewArea / 100)
        
        nWidth = ReaderWindow.oImage.size[0]
        nHeight = ReaderWindow.oImage.size[1]
        ReaderWindow.nOrigWidth = nWidth
        ReaderWindow.nOrigHeight = nHeight
        
        nMultiplier = (nViewWidth * 1.0) / nWidth
        if nWidth > nViewWidth:
            bShrink = True
            ReaderWindow.nOrigWidth = nWidth
            ReaderWindow.nOrigHeight = nHeight
        
        if nMultiplier > 2: nMultiplier = 2
        
        nWidth = int(nWidth * nMultiplier)
        nHeight = int(nHeight * nMultiplier)
        self.DisplayImage(nWidth, nHeight)
        
        ReaderWindow.oImageArea.SetFocus()
        
        if bShrink: ReaderWindow.nImageState = 1
        
        self.Refresh()
        self.Update()

    def OnImageClick(self, event):
        if ReaderWindow.nImageState == 1:
            oScrSize = ReaderWindow.oImageArea.GetSize()
            
            fMult = (ReaderWindow.nOrigWidth * 1.0) / oScrSize.GetWidth()            
            if ReaderWindow.nOrigWidth > ReaderWindow.nOrigHeight:
                nWidth = int(oScrSize.GetWidth() * 0.98)
                nHeight = int(((ReaderWindow.nOrigHeight * 1.0) / fMult) * 0.98)
                self.DisplayImage(nWidth, nHeight)
                ReaderWindow.oImageArea.SetFocus()
                ReaderWindow.nImageState = 0
            else:
                self.OnPageNext(event)
        else:
            self.OnPageNext(event)
        
    def OnPageNext(self, event):
        nChap = ReaderWindow.oChapter.GetSelection()
        nItem = ReaderWindow.oPage.GetSelection()
        if nItem < (len(ReaderWindow.sPageList) - 1):
            ReaderWindow.oPage.SetSelection(nItem + 1)
            self.OnPageSelect(ReaderWindow.oPage)
        else:
            if nChap < (len(ReaderWindow.sChapterList) - 1):
                ReaderWindow.oChapter.SetSelection(nChap + 1)
                self.OnChapterSelect(ReaderWindow.oChapter)
        
    def OnPagePrev(self, event):
        nChap = ReaderWindow.oChapter.GetSelection()
        nItem = ReaderWindow.oPage.GetSelection()
        if nItem > 0:
            ReaderWindow.oPage.SetSelection(nItem - 1)
            self.OnPageSelect(ReaderWindow.oPage)
        else:
            if nChap > 0:
                ReaderWindow.bPrev = True
                ReaderWindow.oChapter.SetSelection(nChap - 1)
                self.OnChapterSelect(ReaderWindow.oChapter)
    
    def OnPageFirst(self, event):
        ReaderWindow.oPage.SetSelection(0)
        self.OnPageSelect(ReaderWindow.oPage)
        
    def OnPageLast(self, event):
        ReaderWindow.oPage.SetSelection(len(ReaderWindow.sPageList) - 1)
        self.OnPageSelect(ReaderWindow.oPage)        
        
    def OnChapPrev(self, event):
        nItem = ReaderWindow.oChapter.GetSelection()
        if nItem > 0:
            ReaderWindow.oChapter.SetSelection(nItem - 1)
            self.OnChapterSelect(ReaderWindow.oChapter)
        
    def OnChapNext(self, event):
        nItem = ReaderWindow.oChapter.GetSelection()
        if nItem < len(ReaderWindow.sChapterList):
            ReaderWindow.oChapter.SetSelection(nItem + 1)
            self.OnChapterSelect(ReaderWindow.oChapter)
        
    def SetStatus(self, *sArgs):
        ReaderWindow.oStatus.SetStatus(*sArgs)
        if not ReaderWindow.oStatus.IsShown():
            ReaderWindow.oStatus.Show()
            
    def HideStatus(self):
        ReaderWindow.oStatus.Hide()
        
class ReaderApp(wx.App):
    def OnInit(self):
        frame = ReaderWindow(None, -1, "Manga Reader")
        frame.Show()
        return True

    def OnClose(self):
        ReaderWindow.Close()

app = ReaderApp(1, "MangaReader.log")
app.MainLoop()
